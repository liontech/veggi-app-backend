from django.contrib import admin
from veggiapp.admin import admin_site
from django.apps import apps
from django.db.models import Q
from .models import Nationality, NationalityPermissionGroup, NationalityPermission, UniversalText, LocalizedText


class LocalizedStackedInline(admin.StackedInline):
    localized_fields = []
    has_nationality = True

    def get_queryset(self, request):
        if self.has_nationality is False:
            return self.model.objects.all()
        nationalities = NationalityPermissionGroup.get_permitted_nationalities(request.user)
        return self.model.objects.filter(Q(nationality__in=nationalities) | Q(nationality=None))

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name is "nationality":
            ids = NationalityPermissionGroup.get_permitted_nationality_ids(request.user)
            kwargs["queryset"] = Nationality.objects.filter(id__in=ids)

        nationalities = NationalityPermissionGroup.get_permitted_nationalities(request.user)
        for localized_field in self.localized_fields:
            if db_field.name is localized_field["name"]:
                model = apps.get_model(model_name=localized_field["model"],
                                       app_label=localized_field["app"])
                kwargs["queryset"] = model.objects.filter(nationality__in=nationalities)

        return super(LocalizedStackedInline, self).formfield_for_foreignkey(db_field, request, **kwargs)


class LocalizedModelAdmin(admin.ModelAdmin):
    localized_fields = []
    has_nationality = True

    def get_queryset(self, request):
        if self.has_nationality is False:
            return self.model.objects.all()
        nationalities = NationalityPermissionGroup.get_permitted_nationalities(request.user)
        return self.model.objects.filter(Q(nationality__in=nationalities) | Q(nationality=None))

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name is "nationality":
            ids = NationalityPermissionGroup.get_permitted_nationality_ids(request.user)
            kwargs["queryset"] = Nationality.objects.filter(id__in=ids)

        nationalities = NationalityPermissionGroup.get_permitted_nationalities(request.user)
        for localized_field in self.localized_fields:
            if db_field.name is localized_field["name"]:
                model = apps.get_model(model_name=localized_field["model"],
                                       app_label=localized_field["app"])
                kwargs["queryset"] = model.objects.filter(nationality__in=nationalities)

        return super(LocalizedModelAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)


class NationalityAdmin(admin.ModelAdmin):
    field = 'title_text'


class NationalityPermissionInline(admin.StackedInline):
    model = NationalityPermission
    extra = 0


class NationalityPermissionGroupAdmin(admin.ModelAdmin):
    field = 'user'
    inlines = [NationalityPermissionInline]


class UniversalTextAdmin(admin.ModelAdmin):
    field = 'title_text'


class LocalizedTextAdmin(LocalizedModelAdmin):
    fields = ('nationality', 'universal_text', 'text')
    list_display = ('text', 'universal_text', 'nationality')

    def get_readonly_fields(self, request, obj=None):
        return ['universal_text', 'nationality']


admin_site.register(Nationality, NationalityAdmin)
admin_site.register(NationalityPermissionGroup, NationalityPermissionGroupAdmin)
admin_site.register(UniversalText, UniversalTextAdmin)
admin_site.register(LocalizedText, LocalizedTextAdmin)