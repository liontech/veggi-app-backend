from django.db import models
from django.contrib.auth.models import User
from versioning.models import VersionedModel


class Nationality(models.Model):

    title_text = models.CharField('title', default='', max_length=4)
    title_long_text = models.CharField('title long', default='', max_length=140)

    def __unicode__(self):
        return self.title_text

    def save(self, **kwargs):
        super(Nationality, self).save()

        from habits.models import UniversalHabitMeta, UniversalUserCategory
        from statistics.models import UniversalStatisticMeta

        def populate_localizations(model_type):
            for universalModel in model_type.objects.all():
                universalModel.populate_localizations()

        populate_localizations(UniversalHabitMeta)
        populate_localizations(UniversalUserCategory)
        populate_localizations(UniversalStatisticMeta)
        populate_localizations(UniversalText)

    def to_json(self):
        return {
            'code': self.title_text,
            'title': self.title_long_text
        }


class NationalityPermissionGroup(models.Model):

    user = models.ForeignKey(User)

    @classmethod
    def get_permitted_nationalities(cls, user):
        permission_group = NationalityPermissionGroup.objects.get_or_create(user=user)[0]
        nationalities = []
        for permission in permission_group.nationalitypermission_set.all():
            nationalities.append(permission.nationality)
        return nationalities

    @classmethod
    def get_permitted_nationality_ids(cls, user):
        nationalities = NationalityPermissionGroup.get_permitted_nationalities(user)
        ids = []
        for nationality in nationalities:
            ids.append(nationality.id)
        return ids

    def __unicode__(self):
        return self.user.username + "'s localization permissions"


class NationalityPermission(models.Model):

    nationality_permission_group = models.ForeignKey(NationalityPermissionGroup)
    nationality = models.ForeignKey(Nationality)

    def __unicode__(self):
        return self.nationality.title_text + " permission"


class LocalizedModel(VersionedModel):

    nationality = models.ForeignKey(Nationality, default=None, null=True)

    class Meta:
        abstract = True


class LocalizedText(LocalizedModel):

    universal_text = models.ForeignKey('UniversalText', default=None, null=True)
    text = models.CharField('text', default='... no translation available ...', blank=True, max_length=400)

    def __unicode__(self):
        return self.text

    @classmethod
    def get_localized_json(cls, nationality):
        localization = {}
        for localizedText in LocalizedText.objects.filter(nationality=nationality):
            localization[localizedText.universal_text.title_text] = localizedText.text
        return localization


class UniversalText(VersionedModel):

    title_text = models.CharField('title', default='', max_length=140)

    def __unicode__(self):
        return self.title_text

    def populate_localizations(self):
        for nationality in Nationality.objects.all():
            LocalizedText.objects.get_or_create(nationality=nationality, universal_text=self)

    def save(self, **kwargs):
        super(UniversalText, self).save()
        self.populate_localizations()