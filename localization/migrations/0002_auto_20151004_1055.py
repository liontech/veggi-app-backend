# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('localization', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='LocalizedTextModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.CharField(default=b'', max_length=400, verbose_name=b'text')),
                ('nationality', models.ForeignKey(default=None, to='localization.Nationality', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='UniversalTextModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title_text', models.CharField(default=b'', max_length=140, verbose_name=b'title')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='localizedtextmodel',
            name='universal_text',
            field=models.ForeignKey(default=None, to='localization.UniversalTextModel', null=True),
        ),
    ]
