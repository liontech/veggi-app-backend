# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('localization', '0002_auto_20151004_1055'),
    ]

    operations = [
        migrations.CreateModel(
            name='LocalizedText',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.CharField(default=b'', max_length=400, verbose_name=b'text')),
                ('nationality', models.ForeignKey(default=None, to='localization.Nationality', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.RenameModel(
            old_name='UniversalTextModel',
            new_name='UniversalText',
        ),
        migrations.RemoveField(
            model_name='localizedtextmodel',
            name='nationality',
        ),
        migrations.RemoveField(
            model_name='localizedtextmodel',
            name='universal_text',
        ),
        migrations.DeleteModel(
            name='LocalizedTextModel',
        ),
        migrations.AddField(
            model_name='localizedtext',
            name='universal_text',
            field=models.ForeignKey(default=None, to='localization.UniversalText', null=True),
        ),
    ]
