# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('localization', '0005_auto_20151004_1750'),
    ]

    operations = [
        migrations.AlterField(
            model_name='localizedtext',
            name='text',
            field=models.CharField(default=b'... no translation available ...', max_length=400, verbose_name=b'text', blank=True),
        ),
    ]
