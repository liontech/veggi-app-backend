# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('localization', '0004_auto_20151004_1239'),
    ]

    operations = [
        migrations.AddField(
            model_name='nationality',
            name='title_long_text',
            field=models.CharField(default=b'', max_length=140, verbose_name=b'title long'),
        ),
        migrations.AlterField(
            model_name='nationality',
            name='title_text',
            field=models.CharField(default=b'', max_length=4, verbose_name=b'title'),
        ),
    ]
