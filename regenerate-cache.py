import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "veggiapp.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()


from veggiapp.utils import get_list_json
from veggiapp.views import get_global_data
from localization.models import Nationality
import json


def save_json(path, data):
    with open(path, 'w') as outfile:
        json.dump(data, outfile)


directory = "cache/"

if not os.path.exists(directory):
    os.makedirs(directory)

for nationality in Nationality.objects.all():
    filename = nationality.title_text + ".json"
    data = get_global_data(nationality.title_text)
    save_json(path=directory + filename, data=data)

filename = "nationalities.json"
data = get_list_json(Nationality.objects.all())
save_json(path=directory + filename, data=data)

print 'caching successful'