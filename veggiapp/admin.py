from django.contrib.admin import AdminSite
from django.contrib import admin
from django.contrib.auth.models import User, Group


class VeggiAppAdminSite(AdminSite):
    site_header = 'Veggi app administration'
    site_title = 'Veggi App Admin'
    index_title = 'Admin'


admin_site = VeggiAppAdminSite()


class UserAdmin(admin.ModelAdmin):
    fields = ('username', 'email', 'group')


admin_site.register(User)
admin_site.register(Group)