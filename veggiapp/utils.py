from rest_framework.authtoken.models import Token
from django.http import HttpResponse
import json, os


def json_response(response_dict, status=200):
    response = HttpResponse(json.dumps(response_dict), content_type="application/json", status=status)
    response['Access-Control-Allow-Origin'] = '*'
    response['Access-Control-Allow-Headers'] = 'Content-Type, Authorization'
    return response


def token_required(func):
    def inner(request, *args, **kwargs):
        if request.method == 'OPTIONS':
            return func(request, *args, **kwargs)
        auth_header = request.META.get('HTTP_AUTHORIZATION', None)
        if auth_header is not None:
            try:
                request.token = Token.objects.get(key=auth_header)
                request.user = request.token.user
                return func(request, *args, **kwargs)
            except Token.DoesNotExist:
                return json_response({
                    'error': 'Token not found'
                }, status=401)
        return json_response({
            'error': 'Invalid Header'
        }, status=401)

    return inner


def get_list_json(objects):
    return [obj.to_json() for obj in objects]


def get_list_strings(objects):
    return [str(obj) for obj in objects]


def get_json_file_response(path):
    if not os.path.exists(path):
        return json_response({
            'msg': 'file does not exist.'
        })

    response = HttpResponse(open(path, "r"))
    response['Content-Disposition'] = "filename=%s" % path
    return response