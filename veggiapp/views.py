from utils import json_response, get_json_file_response
from utils import get_list_json
from articles.models import Category as ArticleCategory, Article, Stage as ArticleDifficulty
from statistics.models import UniversalStatisticMeta
from habits.models import UniversalHabitMeta, UniversalUserCategory
from recipes.models import Recipe, Ingredient, RecipeCategory as RecipeCategory, \
    Measurement, Tool, Duration as RecipeDuration, Order as IngredientOrder
from introduction.models import Page
from versioning.models import Version
from localization.models import Nationality, LocalizedText


def get_global_data(nationality_title):
    nationality_list = Nationality.objects.filter(title_text=nationality_title)

    if not nationality_list:
        return {
            'msg': 'nationality not supported'
        }

    nationality = nationality_list[0]

    return {
        'nationality': nationality.title_text,
        'version': Version.get_latest_version_number(),
        'intro_pages': get_list_json(Page.objects.all().filter(nationality=nationality)),
        'articles': get_list_json(Article.objects.all().filter(nationality=nationality, published_bool=True)),
        'article_stages': get_list_json(ArticleDifficulty.objects.all().filter(nationality=nationality)),
        'article_categories': get_list_json(ArticleCategory.objects.all().filter(nationality=nationality)),
        'statistics': UniversalStatisticMeta.get_localized_json_list(nationality=nationality),
        'habits': UniversalHabitMeta.get_localized_json_list(nationality=nationality),
        'recipes': get_list_json(Recipe.objects.all().filter(nationality=nationality, published_bool=True)),
        'ingredient_orders': get_list_json(IngredientOrder.objects.all().filter(nationality=nationality)),
        'ingredients': get_list_json(Ingredient.objects.all().filter(nationality=nationality)),
        'tools': get_list_json(Tool.objects.all().filter(nationality=nationality)),
        'ingredient_measurements': get_list_json(Measurement.objects.all().filter(nationality=nationality)),
        'recipe_categories': get_list_json(RecipeCategory.objects.all().filter(nationality=nationality)),
        'recipe_durations': get_list_json(RecipeDuration.objects.all().filter(nationality=nationality)),
        'user_categories': UniversalUserCategory.get_localized_json_list(nationality=nationality),
        'translations': LocalizedText.get_localized_json(nationality=nationality),
        'nationalities': get_list_json(Nationality.objects.all())
    }


def index(request, nationality_title):
    path = "cache/" + nationality_title + ".json"
    return get_json_file_response(path=path)


def nationalities(request):
    path = "cache/nationalities.json"
    return get_json_file_response(path=path)