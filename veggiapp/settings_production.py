import os
from confidentials import *

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

DB_ENGINE = 'django.db.backends.mysql'
DB_NAME = 'jorisvanleeuwen$veggiapp'
DB_HOST = 'jorisvanleeuwen.mysql.pythonanywhere-services.com'
DB_USER = DatabaseUser
DB_PASSWORD = DatabasePassword
