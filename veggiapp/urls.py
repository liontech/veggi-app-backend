from django.conf.urls import include, url
from django.conf.urls import patterns, include
from admin import admin_site
from . import views

urlpatterns = [
    url(r'^versioning/', include('versioning.urls')),
    url(r'^admin/', include(admin_site.urls)),
    url(r'^(?P<nationality_title>[a-z]{2})/', views.index, name='index'),
    url(r'^nationalities/$', views.nationalities, name='nationalities'),
]