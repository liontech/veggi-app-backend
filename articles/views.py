from .models import Article
from veggiapp.utils import json_response


def index(request):
    articles = [article.to_json() for article in Article.objects.all()]
    return json_response(articles)


def detail(request, article_id):
    article = Article.objects.filter(id=article_id)[0].to_json()
    return json_response(article)