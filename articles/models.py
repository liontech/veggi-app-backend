from django.db import models
from veggiapp.utils import get_list_json
from versioning.models import VersionedModel
from localization.models import LocalizedModel
from content.models import Image


class Stage(LocalizedModel):
    title_text = models.CharField('title', max_length=200)
    order_int = models.IntegerField('order', default=0, help_text='Starts with zero, higher value will make associated articles appear later.')

    def __unicode__(self):
        return self.title_text

    def to_json(self):
        return {
            'id': self.id,
            'title': self.title_text,
            'order': self.order_int
        }


class Category(LocalizedModel):
    title_text = models.CharField('title', max_length=200)

    def __unicode__(self):
        return self.title_text

    def to_json(self):
        return {
            'id': self.id,
            'title': self.title_text
        }


class Article(LocalizedModel):
    category = models.ForeignKey('Category', null=True, default=None)
    stage = models.ForeignKey('Stage', null=True, default=None)

    title_text = models.CharField('title', max_length=200)
    sub_title_text = models.CharField('sub-title', max_length=200)

    published_bool = models.BooleanField('published', default=True, help_text='Leave this unchecked if you do not want this article to show up in the app yet.')

    def __str__(self):
        return unicode(self.title_text)

    def to_json(self):
        return {
            'id': self.id,
            'title': self.title_text,
            'sub_title': self.sub_title_text,
            'paragraphs': get_list_json(self.paragraph_set.all()),
            'sources': get_list_json(self.source_set.all()),
            'category_id': self.category_id,
            'stage_id': self.stage_id,
            'references': get_list_json(self.reference_set.all())
        }


class Paragraph(VersionedModel):
    article = models.ForeignKey(Article)
    image = models.ForeignKey(Image, default=None, null=True, blank=True, help_text='Optional. When added, the image will appear above the content text.')
    is_special = models.BooleanField('is special', default=False, help_text='Special paragraphs will be highlighted in the article.')
    content_text = models.TextField('content', max_length=1600)

    def to_json(self):
        image_url = ''
        if self.image is not None:
            image_url = self.image.title_text
        return {
            'content': self.content_text,
            'image_url': image_url,
            'is_special': self.is_special
        }


class Reference(VersionedModel):
    article = models.ForeignKey('Article', related_name='reference_set')
    target = models.ForeignKey('Article', related_name='target_set')

    def to_json(self):
        return {
            'article_id': self.target_id
        }


class Source(VersionedModel):
    article = models.ForeignKey(Article)
    title_text = models.CharField('title', max_length=400, help_text='A source that is used to write this article.')

    def to_json(self):
        return self.title_text