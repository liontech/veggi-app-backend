from django.contrib import admin
from veggiapp.admin import admin_site
from .models import Article, Paragraph, Source, Category, Stage, Reference
from localization.admin import LocalizedModelAdmin, LocalizedStackedInline


class ParagraphInline(admin.StackedInline):
    model = Paragraph
    extra = 3


class SourceInline(admin.StackedInline):
    model = Source
    extra = 3


class ReferenceInline(LocalizedStackedInline):
    model = Reference
    fk_name = 'article'
    extra = 0
    has_nationality = False
    localized_fields = [{'name': 'target', 'app': 'articles', 'model': 'Article'}]


class CategoryAdmin(LocalizedModelAdmin):
    fields = ('nationality', 'title_text')
    list_display = ('title_text', 'nationality')


class StageAdmin(LocalizedModelAdmin):
    fields = ('nationality', 'title_text', 'order_int')
    list_display = ('title_text', 'order_int', 'nationality')


class ArticleAdmin(LocalizedModelAdmin):
    fields = ('nationality', 'category', 'stage', 'title_text', 'sub_title_text', 'published_bool')
    inlines = [ParagraphInline, SourceInline, ReferenceInline]
    list_display = ('title_text', 'category', 'stage', 'published_bool', 'nationality')
    search_fields = ['title_text']
    localized_fields = [
        {'name': 'category', 'app': 'articles', 'model': 'Category'},
        {'name': 'stage', 'app': 'articles', 'model': 'Stage'}
    ]


admin_site.register(Category, CategoryAdmin)
admin_site.register(Stage, StageAdmin)
admin_site.register(Article, ArticleAdmin)