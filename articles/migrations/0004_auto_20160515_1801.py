# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0003_auto_20160515_1800'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='published_bool',
            field=models.BooleanField(default=True, help_text=b'Leave this unchecked if you do not want this article to show up in the app yet.', verbose_name=b'published'),
        ),
    ]
