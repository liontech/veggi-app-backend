# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('localization', '__first__'),
        ('content', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title_text', models.CharField(max_length=200, verbose_name=b'title')),
                ('sub_title_text', models.CharField(max_length=200, verbose_name=b'sub-title')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title_text', models.CharField(max_length=200, verbose_name=b'title')),
                ('nationality', models.ForeignKey(default=None, to='localization.Nationality', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Paragraph',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_special', models.BooleanField(default=False, help_text=b'Special paragraphs will be highlighted in the article.', verbose_name=b'is special')),
                ('content_text', models.TextField(max_length=1600, verbose_name=b'content')),
                ('article', models.ForeignKey(to='articles.Article')),
                ('image', models.ForeignKey(default=None, blank=True, to='content.Image', help_text=b'Optional. When added, the image will appear above the content text.', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Reference',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('article', models.ForeignKey(related_name='reference_set', to='articles.Article')),
                ('target', models.ForeignKey(related_name='target_set', to='articles.Article')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Source',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title_text', models.CharField(help_text=b'A source that is used to write this article.', max_length=400, verbose_name=b'title')),
                ('article', models.ForeignKey(to='articles.Article')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Stage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title_text', models.CharField(max_length=200, verbose_name=b'title')),
                ('order_int', models.IntegerField(default=0, help_text=b'Starts with zero, higher value will make associated articles appear later.', verbose_name=b'order')),
                ('nationality', models.ForeignKey(default=None, to='localization.Nationality', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='article',
            name='category',
            field=models.ForeignKey(default=None, to='articles.Category', null=True),
        ),
        migrations.AddField(
            model_name='article',
            name='nationality',
            field=models.ForeignKey(default=None, to='localization.Nationality', null=True),
        ),
        migrations.AddField(
            model_name='article',
            name='stage',
            field=models.ForeignKey(default=None, to='articles.Stage', null=True),
        ),
    ]
