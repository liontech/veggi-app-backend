from django.db import models
from habits.models import UniversalHabitMeta
from veggiapp.utils import get_list_json
from versioning.models import VersionedModel
from localization.models import LocalizedModel, Nationality
from content.models import Icon, Image
from articles.models import Article


class UniversalStatisticMeta(VersionedModel):

    title_text = models.CharField('title', max_length=200, default='')

    @classmethod
    def get_localized_json_list(cls, nationality):
        json_list = []
        for universal_statistic_meta in UniversalStatisticMeta.objects.all():
            statistic_meta_set = universal_statistic_meta.statisticmeta_set
            statistic_meta = statistic_meta_set.filter(universal_statistic_meta=universal_statistic_meta,
                                                       nationality=nationality)
            if not statistic_meta:
                continue
            json_list.append({
                'id': universal_statistic_meta.id,
                'title': statistic_meta[0].title_text,
                'references': get_list_json(statistic_meta[0].reference_set.all()),
                'basic_unit': statistic_meta[0].basic_unit_text,
                'basic_unit_postfix': statistic_meta[0].basic_unit_postfix_text,
                'postfix': statistic_meta[0].postfix_text,
                'info': statistic_meta[0].info_text,
                'icon': statistic_meta[0].icon.title_text,
                'components': get_list_json(statistic_meta[0].statisticcomponent_set.all()),
                'units': get_list_json(statistic_meta[0].unit_set.all())
            })
        return json_list

    def __unicode__(self):
        return self.title_text

    def populate_localizations(self):
        for nationality in Nationality.objects.all():
            StatisticMeta.objects.get_or_create(nationality=nationality, universal_statistic_meta=self)

    def save(self, **kwargs):
        super(UniversalStatisticMeta, self).save()
        self.populate_localizations()


class StatisticMeta(LocalizedModel):

    universal_statistic_meta = models.ForeignKey(UniversalStatisticMeta, default=None, null=True)
    title_text = models.CharField('title', max_length=200, default='... no translation available ...')
    basic_unit_text = models.CharField('basic unit', max_length=200, default='', help_text='The unit that stands for the basic value of this statistic (e.g. "kg").')
    basic_unit_postfix_text = models.CharField('basic unit postfix', max_length=200, default='', help_text='Text that follows the basic unit text (e.g. "meat saved").')
    postfix_text = models.CharField('postfix', max_length=200, default='', help_text='The text that follows a unit text (e.g. "saved").')
    info_text = models.TextField('info', max_length=1600, default='')
    icon = models.ForeignKey(Icon, default=None, null=True)

    def __str__(self):
        return self.title_text


class ArticleReference(VersionedModel):
    statistic_meta = models.ForeignKey('StatisticMeta', related_name='reference_set')
    target = models.ForeignKey('articles.Article', related_name='statistic_target_set')

    def to_json(self):
        return {
            'article_id': self.target_id
        }


class StatisticComponent(VersionedModel):

    statistic_meta = models.ForeignKey(StatisticMeta, default=None)
    universal_habit_meta = models.ForeignKey(UniversalHabitMeta, default=None)
    factor_float = models.FloatField('factor', default=1, help_text='Amount of value added per day when no days are selected for this habit. '
                                                                    'Zero is added when user has the habit set to or higher than the average day amount.')

    def to_json(self):
        return {
            'factor': self.factor_float,
            'habit_id': self.universal_habit_meta_id
        }


class Unit(VersionedModel):

    statistic_meta = models.ForeignKey(StatisticMeta, default=None)
    title_zero_text = models.CharField('zero text', default='', max_length=300, help_text='The text that follows a value of zero (e.g. "chickens").')
    title_single_text = models.CharField('single text', default='', max_length=300, help_text='The text that follows a value of one (e.g. "chicken").')
    title_plural_text = models.CharField('plural text', default='', max_length=300, help_text='The text that follows a value of 2 or higher (e.g. "chickens").')
    factor_float = models.FloatField('factor', default=1, help_text="The statistic's value multiplied by this factor results in the final value of this unit.")
    icon = models.ForeignKey(Icon)

    def __unicode__(self):
        return self.title_plural_text

    def to_json(self):
        return ({
            'id': self.id,
            'title_zero': self.title_zero_text,
            'title_single': self.title_single_text,
            'title_plural': self.title_plural_text,
            'factor': self.factor_float,
            'icon': self.icon.title_text
        })