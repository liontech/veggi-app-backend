from django.contrib import admin
from veggiapp.admin import admin_site
from .models import StatisticMeta, StatisticComponent, Unit, UniversalStatisticMeta, ArticleReference
from localization.admin import LocalizedModelAdmin, LocalizedStackedInline


class UniversalStatisticMetaAdmin(admin.ModelAdmin):
    field = 'title_text'


class StatisticComponentInline(admin.StackedInline):
    model = StatisticComponent
    extra = 0


class UnitInline(admin.StackedInline):
    model = Unit
    extra = 0


class ArticleReferenceInline(LocalizedStackedInline):
    model = ArticleReference
    fk_name = 'statistic_meta'
    extra = 0
    has_nationality = False
    localized_fields = [{'name': 'target', 'app': 'articles', 'model': 'Article'}]


class StatisticMetaAdmin(LocalizedModelAdmin):
    fields = ('nationality', 'universal_statistic_meta', 'title_text', 'basic_unit_text', 'basic_unit_postfix_text', 'postfix_text', 'info_text', 'icon')
    inlines = [StatisticComponentInline, UnitInline, ArticleReferenceInline]
    list_display = ('title_text', 'universal_statistic_meta', 'nationality')

    def get_readonly_fields(self, request, obj=None):
        return ['universal_statistic_meta', 'nationality']


admin_site.register(UniversalStatisticMeta, UniversalStatisticMetaAdmin)
admin_site.register(StatisticMeta, StatisticMetaAdmin)