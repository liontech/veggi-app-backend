# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('statistics', '0003_auto_20151004_1239'),
    ]

    operations = [
        migrations.AlterField(
            model_name='statisticmeta',
            name='title_text',
            field=models.CharField(default=b'... no translation available ...', max_length=200, verbose_name=b'title'),
        ),
    ]
