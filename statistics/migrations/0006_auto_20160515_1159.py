# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('statistics', '0005_auto_20160514_1904'),
    ]

    operations = [
        migrations.AlterField(
            model_name='statisticmeta',
            name='basic_unit_postfix_text',
            field=models.CharField(default=b'', help_text=b'Text that follows the basic unit text (e.g. "meat saved").', max_length=200, verbose_name=b'basic unit postfix'),
        ),
    ]
