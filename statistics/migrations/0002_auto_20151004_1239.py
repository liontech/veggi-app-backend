# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('statistics', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='statisticmeta',
            name='icon',
            field=models.ForeignKey(default=None, blank=True, to='content.Icon', null=True),
        ),
    ]
