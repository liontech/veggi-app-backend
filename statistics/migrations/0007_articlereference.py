# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0001_initial'),
        ('statistics', '0006_auto_20160515_1159'),
    ]

    operations = [
        migrations.CreateModel(
            name='ArticleReference',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('statistic_meta', models.ForeignKey(related_name='reference_set', to='statistics.StatisticMeta')),
                ('target', models.ForeignKey(related_name='statistic_target_set', to='articles.Article')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
