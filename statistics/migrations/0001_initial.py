# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('localization', '0001_initial'),
        ('habits', '0001_initial'),
        ('content', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='StatisticComponent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('factor_float', models.FloatField(default=1, help_text=b'Amount of value added per day when no days are selected for this habit. Zero is added when user has the habit set to or higher than the average day amount.', verbose_name=b'factor')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='StatisticMeta',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title_text', models.CharField(default=b'', max_length=200, verbose_name=b'title')),
                ('basic_unit_text', models.CharField(default=b'', help_text=b'The unit that stands for the basic value of this statistic (e.g. "kg meat saved").', max_length=200, verbose_name=b'basic unit')),
                ('postfix_text', models.CharField(default=b'', help_text=b'The text that follows a unit text (e.g. "saved").', max_length=200, verbose_name=b'postfix')),
                ('info_text', models.TextField(default=b'', max_length=1600, verbose_name=b'info')),
                ('icon', models.ForeignKey(to='content.Icon')),
                ('nationality', models.ForeignKey(default=None, to='localization.Nationality', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Unit',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title_zero_text', models.CharField(default=b'', help_text=b'The text that follows a value of zero (e.g. "chickens").', max_length=300, verbose_name=b'zero text')),
                ('title_single_text', models.CharField(default=b'', help_text=b'The text that follows a value of one (e.g. "chicken").', max_length=300, verbose_name=b'single text')),
                ('title_plural_text', models.CharField(default=b'', help_text=b'The text that follows a value of 2 or higher (e.g. "chickens").', max_length=300, verbose_name=b'plural text')),
                ('factor_float', models.FloatField(default=1, help_text=b"The statistic's value multiplied by this factor results in the final value of this unit.", verbose_name=b'factor')),
                ('icon', models.ForeignKey(to='content.Icon')),
                ('statistic_meta', models.ForeignKey(default=None, to='statistics.StatisticMeta')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='UniversalStatisticMeta',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title_text', models.CharField(default=b'', max_length=200, verbose_name=b'title')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='statisticmeta',
            name='universal_statistic_meta',
            field=models.ForeignKey(default=None, to='statistics.UniversalStatisticMeta', null=True),
        ),
        migrations.AddField(
            model_name='statisticcomponent',
            name='statistic_meta',
            field=models.ForeignKey(default=None, to='statistics.StatisticMeta'),
        ),
        migrations.AddField(
            model_name='statisticcomponent',
            name='universal_habit_meta',
            field=models.ForeignKey(default=None, to='habits.UniversalHabitMeta'),
        ),
    ]
