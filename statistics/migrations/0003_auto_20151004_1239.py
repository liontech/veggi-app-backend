# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('statistics', '0002_auto_20151004_1239'),
    ]

    operations = [
        migrations.AlterField(
            model_name='statisticmeta',
            name='icon',
            field=models.ForeignKey(default=None, to='content.Icon', null=True),
        ),
    ]
