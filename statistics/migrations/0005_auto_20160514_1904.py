# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('statistics', '0004_auto_20151004_1318'),
    ]

    operations = [
        migrations.AddField(
            model_name='statisticmeta',
            name='basic_unit_postfix_text',
            field=models.CharField(default=b'', help_text=b'Text that follows the basic unit text (e.g. "meat saved").', max_length=200, verbose_name=b'basic_unit_postfix'),
        ),
        migrations.AlterField(
            model_name='statisticmeta',
            name='basic_unit_text',
            field=models.CharField(default=b'', help_text=b'The unit that stands for the basic value of this statistic (e.g. "kg").', max_length=200, verbose_name=b'basic unit'),
        ),
    ]
