from django.contrib import admin
from veggiapp.admin import admin_site
from models import Version


class VersionAdmin(admin.ModelAdmin):
    field = 'number_int'


admin_site.register(Version, VersionAdmin)