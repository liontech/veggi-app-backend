from django.db import models
from django.db.models.signals import post_delete
from django.dispatch.dispatcher import receiver


class Version(models.Model):

    number_int = models.IntegerField('number', default=0)

    def __unicode__(self):
        return "version " + str(self.number_int)

    def to_json(self):
        return {
            'number': self.number_int
        }

    @classmethod
    def get_latest_version_number(cls):
        version = Version.objects.get_or_create()[0]
        return version.number_int

    @classmethod
    def increase_version(cls):
        version = Version.objects.get_or_create()[0]
        version.number_int += 1
        version.save()


class VersionedModel(models.Model):

    class Meta:
        abstract = True

    def save(self, **kwargs):
        Version.increase_version()
        super(VersionedModel, self).save()


@receiver(post_delete)
def versioned_model_delete(sender, instance, **kwargs):
    if issubclass(sender, VersionedModel):
        Version.increase_version()