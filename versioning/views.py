from veggiapp.utils import json_response
from models import Version


def index(request):
    return json_response({
        'version': Version.get_latest_version_number()
    })