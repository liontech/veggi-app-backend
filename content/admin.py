from django.contrib import admin
from veggiapp.admin import admin_site
from models import Icon, Image


class IconAdmin(admin.ModelAdmin):
    field = 'title_text'

    def has_delete_permission(self, request, obj=None):
        if request.user.is_superuser:
            return True
        if obj is None:
            return False
        return obj.get_ref_count() is 0


class ImageAdmin(admin.ModelAdmin):
    field = 'title_text'

    def has_delete_permission(self, request, obj=None):
        if request.user.is_superuser:
            return True
        if obj is None:
            return False
        return obj.get_ref_count() is 0


admin_site.register(Icon, IconAdmin)
admin_site.register(Image, ImageAdmin)