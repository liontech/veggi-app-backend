# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='icon',
            name='url',
            field=models.CharField(default=b'', max_length=200, verbose_name=b'url', blank=True),
        ),
        migrations.AlterField(
            model_name='image',
            name='url',
            field=models.CharField(default=b'', max_length=200, verbose_name=b'url', blank=True),
        ),
    ]
