# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Icon',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title_text', models.CharField(default=b'', max_length=200, verbose_name=b'title')),
                ('url', models.CharField(default=b'', max_length=200, verbose_name=b'url')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title_text', models.CharField(default=b'', max_length=200, verbose_name=b'title')),
                ('url', models.CharField(default=b'', max_length=200, verbose_name=b'url')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
