# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0003_auto_20160515_1159'),
    ]

    operations = [
        migrations.AlterField(
            model_name='icon',
            name='title_text',
            field=models.CharField(default=b'', help_text=b'Name of icon as stored in the content/icons folder in the app.', max_length=200, verbose_name=b'title'),
        ),
        migrations.AlterField(
            model_name='image',
            name='title_text',
            field=models.CharField(default=b'', help_text=b'Name of image as stored in cloudinary.', max_length=200, verbose_name=b'title'),
        ),
    ]
