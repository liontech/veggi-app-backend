# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0002_auto_20151004_1055'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='icon',
            name='url',
        ),
        migrations.RemoveField(
            model_name='image',
            name='url',
        ),
    ]
