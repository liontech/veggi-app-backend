from django.db import models
from versioning.models import VersionedModel


def formulate_count(count):
    if count is 1:
        return str(count) + " time"
    return str(count) + " times"


class Icon(VersionedModel):

    title_text = models.CharField('title', default='', max_length=200, help_text="Name of icon as stored in the content/icons folder in the app.")

    def __unicode__(self):
        return self.title_text

    def get_ref_count(self):
        return self.universalhabitmeta_set.count() + self.statisticmeta_set.count() + self.unit_set.count()

    def __unicode__(self):
        ref_count = self.get_ref_count()
        return self.title_text + " (used " + formulate_count(ref_count) + ")"


class Image(VersionedModel):

    title_text = models.CharField('title', default='', max_length=200, help_text="Name of image as stored in cloudinary.")

    def get_ref_count(self):
        return self.recipe_set.count() + self.paragraph_set.count() + self.page_set.count()

    def __unicode__(self):
        ref_count = self.get_ref_count()
        return self.title_text + " (used " + formulate_count(ref_count) + ")"