from django.contrib import admin
from veggiapp.admin import admin_site
from localization.admin import LocalizedModelAdmin, LocalizedStackedInline
from models import RecipeCategory, Recipe, Supply, Ingredient, \
    Measurement, Unit, Step, Duration, \
    Tip, Requirement, Tool, Order


class RecipeCategoryAdmin(LocalizedModelAdmin):
    fields = ('nationality', 'title_text', 'index_int')
    list_display = ('title_text', 'index_int', 'nationality')


class DurationAdmin(LocalizedModelAdmin):
    fields = ('nationality', 'title_text', 'minutes_int')
    list_display = ('title_text', 'nationality')


class SupplyInline(LocalizedStackedInline):
    model = Supply
    extra = 0
    has_nationality = False
    localized_fields = [
        {'name': 'ingredient', 'app': 'recipes', 'model': 'Ingredient'},
    ]


class RequirementInline(LocalizedStackedInline):
    model = Requirement
    extra = 0
    has_nationality = False
    localized_fields = [
        {'name': 'tool', 'app': 'recipes', 'model': 'Tool'},
    ]


class StepInline(admin.StackedInline):
    model = Step
    extra = 0


class TipInline(admin.StackedInline):
    model = Tip
    extra = 0


class RecipeAdmin(LocalizedModelAdmin):
    fields = ('nationality', 'recipe_category', 'duration',
              'title_text', 'published_bool', 'description_text',
              'image', 'person_count_int', 'difficulty', 'status_text')
    inlines = [SupplyInline, RequirementInline, StepInline, TipInline]
    list_display = ('title_text', 'is_vegan', 'published_bool', 'nationality')
    localized_fields = [
        {'name': 'recipe_category', 'app': 'recipes', 'model': 'RecipeCategory'},
        {'name': 'duration', 'app': 'recipes', 'model': 'Duration'}
    ]

    def is_vegan(self, obj):
        return obj.is_vegan()
    is_vegan.short_description = 'Is vegan'
    is_vegan.admin_order_field = 'is_vegan'


class OrderAdmin(LocalizedModelAdmin):
    fields = ('nationality', 'title_text', 'order_int')
    list_display = ('title_text', 'order_int', 'nationality')


class IngredientAdmin(LocalizedModelAdmin):
    fields = ('nationality', 'order', 'title_text', 'measurement', 'price_float', 'is_vegan')
    list_display = ('title_text', 'is_vegan', 'nationality', 'order')
    localized_fields = [
        {'name': 'measurement', 'app': 'recipes', 'model': 'Measurement'},
        {'name': 'order', 'app': 'recipes', 'model': 'Order'}
    ]


class ToolAdmin(LocalizedModelAdmin):
    fields = ('nationality', 'title_text')
    list_display = ('title_text', 'nationality')


class UnitInline(admin.StackedInline):
    model = Unit
    extra = 0


class MeasurementAdmin(LocalizedModelAdmin):
    fields = ('nationality', 'title_text')
    inlines = [UnitInline]
    list_display = ('title_text', 'nationality')


admin_site.register(RecipeCategory, RecipeCategoryAdmin)
admin_site.register(Duration, DurationAdmin)
admin_site.register(Recipe, RecipeAdmin)
admin_site.register(Order, OrderAdmin)
admin_site.register(Ingredient, IngredientAdmin)
admin_site.register(Tool, ToolAdmin)
admin_site.register(Measurement, MeasurementAdmin)