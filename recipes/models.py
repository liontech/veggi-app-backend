from django.db import models
from django.contrib.auth.models import User
from veggiapp.utils import get_list_json, get_list_strings
from versioning.models import VersionedModel
from localization.models import LocalizedModel
from content.models import Image


class RecipeCategory(LocalizedModel):

    title_text = models.CharField('title', max_length=200)
    index_int = models.IntegerField('index', default=0, null=False)

    def __unicode__(self):
        return self.title_text

    def to_json(self):
        return {
            'id': self.id,
            'title': self.title_text,
            'index': self.index_int
        }


class Duration(LocalizedModel):

    title_text = models.CharField('title', max_length=200)
    minutes_int = models.IntegerField('minutes')

    def __unicode__(self):
        return self.title_text + " (" + str(self.minutes_int) + " min)"

    def to_json(self):
        return {
            'id': self.id,
            'title': self.title_text,
            'minutes': self.minutes_int
        }


class Recipe(LocalizedModel):

    recipe_category = models.ForeignKey(RecipeCategory, default=None, null=True)
    duration = models.ForeignKey(Duration, default=None)
    person_count_int = models.IntegerField('target amount of people', default=2, help_text='Target amount of people balanced based on the amount of ingredients.')
    title_text = models.CharField('title', max_length=200)
    published_bool = models.BooleanField('published', default=True, help_text='Leave this unchecked if you do not want this recipe to show up in the app yet.')
    image = models.ForeignKey(Image)
    description_text = models.CharField('description', max_length=300)
    status_text = models.CharField('status', max_length=200, default='', blank=True, help_text='A short description of this recipe its status (e.g. "still needs a proper photo")')
    difficulty = models.IntegerField('difficulty', default=0)

    def __unicode__(self):
        return self.title_text

    def get_price(self):
        price = 0
        for supply in self.supply_set.all():
            price += supply.get_price()
        return price

    def is_vegan(self):
        for supply in self.supply_set.all():
            if supply.ingredient.is_vegan is False:
                return False
        return True

    def to_json(self):
        return {
            'id': self.id,
            'category_id': self.recipe_category.id,
            'duration_id': self.duration.id,
            'title': self.title_text,
            'image': self.image.title_text,
            'description': self.description_text,
            'difficulty': self.difficulty,
            'supplies': get_list_json(self.supply_set.all()),
            'requirements': get_list_json(self.requirement_set.all()),
            'steps': get_list_strings(self.step_set.all()),
            'tips': get_list_strings(self.tip_set.all()),
            'price': self.get_price(),
            'person_count': self.person_count_int,
            'status': self.status_text,
            'is_vegan': self.is_vegan()
        }


class Step(VersionedModel):

    recipe = models.ForeignKey(Recipe)
    instructions_text = models.TextField('instructions', max_length=1600)

    def __unicode__(self):
        return self.instructions_text


class Tip(VersionedModel):

    recipe = models.ForeignKey(Recipe)
    tip_text = models.TextField('tip', max_length=1600)

    def __unicode__(self):
        return self.tip_text


class Measurement(LocalizedModel):

    title_text = models.CharField('title', max_length=200)

    def __unicode__(self):
        return self.title_text

    def to_text(self, quantity):
        best_unit = None
        for unit in self.unit_set.all():
            if best_unit is None:
                best_unit = unit
            elif len(unit.to_text(quantity)) < len(best_unit.to_text(quantity)):
                best_unit = unit
        if best_unit is None:
            return "No unit available"
        return best_unit.to_text(quantity) + " " + best_unit.title_text

    def to_json(self):
        return {
            'id': self.id,
            'title': self.title_text,
            'units': get_list_json(self.unit_set.all())
        }


class Unit(VersionedModel):

    measurement = models.ForeignKey(Measurement)
    title_text = models.CharField('title', max_length=200)
    factor_float = models.FloatField('factor', default=1)

    def __unicode__(self):
        return self.title_text

    def to_text(self, quantity):
        value = self.factor_float * quantity
        if value < 1:
            return str(value)
        return "%g" % round(value, 1)

    def to_json(self):
        return {
            'title': self.title_text,
            'factor': self.factor_float
        }


class Order(LocalizedModel):
    title_text = models.CharField('title', max_length=200)
    order_int = models.IntegerField('order', default=0, help_text='Starts with zero, higher value will make associated ingredient appear later.')

    def __unicode__(self):
        return self.title_text

    def to_json(self):
        return {
            'id': self.id,
            'title': self.title_text,
            'order': self.order_int
        }


class Ingredient(LocalizedModel):

    order = models.ForeignKey(Order, null=True)
    measurement = models.ForeignKey(Measurement)
    title_text = models.CharField('title', max_length=200)
    price_float = models.FloatField('price', default=1)
    is_vegan = models.BooleanField('is vegan', default=False)

    def __unicode__(self):
        return self.title_text

    def to_json(self):
        return {
            'id': self.id,
            'title': self.title_text,
            'price': self.price_float,
            'measurement_id': self.measurement.id,
            'is_vegan': self.is_vegan,
            'order_id': self.order_id
        }

    def to_text(self, quantity):
        return self.measurement.to_text(quantity) + " " + self.title_text


class Supply(VersionedModel):

    recipe = models.ForeignKey(Recipe)
    ingredient = models.ForeignKey(Ingredient)
    quantity_float = models.FloatField('quantity', default=1)

    def __unicode__(self):
        return self.ingredient.to_text(quantity=self.quantity_float)

    def get_price(self):
        return self.ingredient.price_float * self.quantity_float

    def to_json(self):
        return {
            'ingredient_id': self.ingredient.id,
            'quantity': self.quantity_float
        }


class Tool(LocalizedModel):

    title_text = models.CharField('title', max_length=200)

    def __unicode__(self):
        return self.title_text

    def to_json(self):
        return {
            'title': self.title_text,
            'id': self.id
        }


class Requirement(VersionedModel):

    recipe = models.ForeignKey(Recipe)
    tool = models.ForeignKey(Tool)

    def to_json(self):
        return {
            'tool_id': self.tool.id
        }