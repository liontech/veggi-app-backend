# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('localization', '0006_auto_20151005_1651'),
        ('recipes', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title_text', models.CharField(max_length=200, verbose_name=b'title')),
                ('order_int', models.IntegerField(default=0, help_text=b'Starts with zero, higher value will make associated ingredient/tool appear later.', verbose_name=b'order')),
                ('nationality', models.ForeignKey(default=None, to='localization.Nationality', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='ingredient',
            name='order',
            field=models.ForeignKey(to='recipes.Order', null=True),
        ),
        migrations.AddField(
            model_name='tool',
            name='order',
            field=models.ForeignKey(to='recipes.Order', null=True),
        ),
    ]
