# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('localization', '0001_initial'),
        ('content', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Duration',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title_text', models.CharField(max_length=200, verbose_name=b'title')),
                ('minutes_int', models.IntegerField(verbose_name=b'minutes')),
                ('nationality', models.ForeignKey(default=None, to='localization.Nationality', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Ingredient',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title_text', models.CharField(max_length=200, verbose_name=b'title')),
                ('price_float', models.FloatField(default=1, verbose_name=b'price')),
                ('is_vegan', models.BooleanField(default=False, verbose_name=b'is vegan')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Measurement',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title_text', models.CharField(max_length=200, verbose_name=b'title')),
                ('nationality', models.ForeignKey(default=None, to='localization.Nationality', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Recipe',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('person_count_int', models.IntegerField(default=2, help_text=b'Target amount of people balanced based on the amount of ingredients.', verbose_name=b'target amount of people')),
                ('title_text', models.CharField(max_length=200, verbose_name=b'title')),
                ('description_text', models.CharField(max_length=300, verbose_name=b'description')),
                ('status_text', models.CharField(default=b'', help_text=b'A short description of this recipe its status (e.g. "still needs a proper photo")', max_length=200, verbose_name=b'status', blank=True)),
                ('difficulty', models.IntegerField(default=0, verbose_name=b'difficulty')),
                ('duration', models.ForeignKey(default=None, to='recipes.Duration')),
                ('image', models.ForeignKey(to='content.Image')),
                ('nationality', models.ForeignKey(default=None, to='localization.Nationality', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='RecipeCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title_text', models.CharField(max_length=200, verbose_name=b'title')),
                ('nationality', models.ForeignKey(default=None, to='localization.Nationality', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Requirement',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('recipe', models.ForeignKey(to='recipes.Recipe')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Step',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('instructions_text', models.TextField(max_length=1600, verbose_name=b'instructions')),
                ('recipe', models.ForeignKey(to='recipes.Recipe')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Supply',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('quantity_float', models.FloatField(default=1, verbose_name=b'quantity')),
                ('ingredient', models.ForeignKey(to='recipes.Ingredient')),
                ('recipe', models.ForeignKey(to='recipes.Recipe')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Tip',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tip_text', models.TextField(max_length=1600, verbose_name=b'tip')),
                ('recipe', models.ForeignKey(to='recipes.Recipe')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Tool',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title_text', models.CharField(max_length=200, verbose_name=b'title')),
                ('nationality', models.ForeignKey(default=None, to='localization.Nationality', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Unit',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title_text', models.CharField(max_length=200, verbose_name=b'title')),
                ('factor_float', models.FloatField(default=1, verbose_name=b'factor')),
                ('measurement', models.ForeignKey(to='recipes.Measurement')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='requirement',
            name='tool',
            field=models.ForeignKey(to='recipes.Tool'),
        ),
        migrations.AddField(
            model_name='recipe',
            name='recipe_category',
            field=models.ForeignKey(default=None, to='recipes.RecipeCategory', null=True),
        ),
        migrations.AddField(
            model_name='ingredient',
            name='measurement',
            field=models.ForeignKey(to='recipes.Measurement'),
        ),
        migrations.AddField(
            model_name='ingredient',
            name='nationality',
            field=models.ForeignKey(default=None, to='localization.Nationality', null=True),
        ),
    ]
