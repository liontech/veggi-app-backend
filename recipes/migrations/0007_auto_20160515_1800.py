# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0006_recipe_published_bool'),
    ]

    operations = [
        migrations.AlterField(
            model_name='recipe',
            name='published_bool',
            field=models.BooleanField(default=False, help_text=b'Leave this unchecked if you do not want this recipe to show up in the app yet.', verbose_name=b'published'),
        ),
    ]
