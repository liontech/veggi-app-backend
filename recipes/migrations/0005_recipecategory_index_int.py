# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0004_auto_20151122_1116'),
    ]

    operations = [
        migrations.AddField(
            model_name='recipecategory',
            name='index_int',
            field=models.IntegerField(default=0, verbose_name=b'index'),
        ),
    ]
