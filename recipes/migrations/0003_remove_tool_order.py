# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0002_auto_20151122_1107'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='tool',
            name='order',
        ),
    ]
