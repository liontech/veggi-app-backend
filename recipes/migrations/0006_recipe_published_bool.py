# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0005_recipecategory_index_int'),
    ]

    operations = [
        migrations.AddField(
            model_name='recipe',
            name='published_bool',
            field=models.BooleanField(default=False, verbose_name=b'published'),
        ),
    ]
