# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0003_remove_tool_order'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='order_int',
            field=models.IntegerField(default=0, help_text=b'Starts with zero, higher value will make associated ingredient appear later.', verbose_name=b'order'),
        ),
    ]
