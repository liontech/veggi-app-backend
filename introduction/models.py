from django.db import models
from localization.models import LocalizedModel
from versioning.models import VersionedModel
from content.models import Image


class UniversalPageType(VersionedModel):

    title_text = models.CharField('title', max_length=200)

    def __unicode__(self):
        return self.title_text


class Page(LocalizedModel):

    title_text = models.CharField('title', max_length=200)
    show_title = models.BooleanField('show title', default=True)
    index_int = models.IntegerField('page index', default=0)
    content_text = models.TextField('content text', max_length=1600)
    page_type = models.ForeignKey(UniversalPageType, default=None, null=True)
    image = models.ForeignKey(Image, default=None, null=True, blank=True, help_text='Leave blank if not selected "image" as page type.')

    def __unicode__(self):
        return self.title_text

    def to_json(self):
        image_url = ''
        if self.image is not None:
            image_url = self.image.title_text

        return {
            'id': self.id,
            'title': self.title_text,
            'show_title': self.show_title,
            'index': self.index_int,
            'page_type': self.page_type.title_text,
            'content_text': self.content_text,
            'image_url': image_url
        }