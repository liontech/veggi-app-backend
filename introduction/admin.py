from django.contrib import admin
from veggiapp.admin import admin_site
from .models import Page, UniversalPageType
from localization.admin import LocalizedModelAdmin


class UniversalPageTypeAdmin(admin.ModelAdmin):
    field = 'title_text'


class PageAdmin(LocalizedModelAdmin):
    fields = ('nationality', 'title_text', 'index_int', 'show_title', 'content_text', 'page_type', 'image')
    list_display = ('title_text', 'index_int', 'nationality')


admin_site.register(Page, PageAdmin)
admin_site.register(UniversalPageType, UniversalPageTypeAdmin)