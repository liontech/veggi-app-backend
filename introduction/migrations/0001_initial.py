# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('localization', '__first__'),
        ('content', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Page',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title_text', models.CharField(max_length=200, verbose_name=b'title')),
                ('show_title', models.BooleanField(default=True, verbose_name=b'show title')),
                ('index_int', models.IntegerField(default=0, verbose_name=b'page index')),
                ('content_text', models.TextField(max_length=1600, verbose_name=b'content text')),
                ('image', models.ForeignKey(default=None, blank=True, to='content.Image', help_text=b'Leave blank if not selected "image" as page type.', null=True)),
                ('nationality', models.ForeignKey(default=None, to='localization.Nationality', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='UniversalPageType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title_text', models.CharField(max_length=200, verbose_name=b'title')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='page',
            name='page_type',
            field=models.ForeignKey(default=None, to='introduction.UniversalPageType', null=True),
        ),
    ]
