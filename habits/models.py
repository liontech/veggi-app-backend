from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from veggiapp.utils import get_list_json
from localization.models import LocalizedModel, Nationality
from versioning.models import VersionedModel
from content.models import Icon


class UniversalHabitMeta(VersionedModel):

    title_text = models.CharField('title', max_length=200, default='')
    icon = models.ForeignKey(Icon, default=None, null=True)

    @classmethod
    def get_localized_json_list(cls, nationality):
        json_list = []
        for universal_habit_meta in UniversalHabitMeta.objects.all():
            habit_meta_set = universal_habit_meta.habitmeta_set
            habit_meta = habit_meta_set.filter(universal_habit_meta=universal_habit_meta,
                                               nationality=nationality)
            if not habit_meta:
                continue
            json_list.append({
                'id': universal_habit_meta.id,
                'icon': universal_habit_meta.icon.title_text,
                'title': habit_meta[0].title_text,
                'universal_title': universal_habit_meta.title_text,
                'info': habit_meta[0].info_text,
                'average_value': habit_meta[0].average_value_int
            })
        return json_list

    def __str__(self):
        return self.title_text

    def populate_localizations(self):
        for nationality in Nationality.objects.all():
            HabitMeta.objects.get_or_create(nationality=nationality, universal_habit_meta=self)[0]

    def save(self, **kwargs):
        super(UniversalHabitMeta, self).save()
        self.populate_localizations()


class HabitMeta(LocalizedModel):

    universal_habit_meta = models.ForeignKey(UniversalHabitMeta, default=None, null=True)
    title_text = models.CharField('title', max_length=200, default='... no translation available ...')
    info_text = models.TextField('info',  max_length=1600)
    average_value_int = models.IntegerField('average day count', default=3,
                                            validators=[MinValueValidator(0), MaxValueValidator(7)],
                                            help_text='The amount of days per week an average person would perform this habit.')

    def __str__(self):
        return self.title_text


class UniversalUserCategory(VersionedModel):

    title_text = models.CharField('title', max_length=200)
    priority_int = models.IntegerField('priority', default=0)

    @classmethod
    def get_localized_json_list(cls, nationality):
        json_list = []
        for universal_user_category in UniversalUserCategory.objects.all():
            user_category_set = universal_user_category.usercategory_set
            user_category = user_category_set.filter(universal_user_category=universal_user_category,
                                                     nationality=nationality)
            if not user_category:
                continue
            json_list.append({
                'id': universal_user_category.id,
                'priority': universal_user_category.priority_int,
                'components': get_list_json(universal_user_category.usercategorycomponent_set.all()),
                'title': user_category[0].title_text,
            })
        return json_list

    def __unicode__(self):
        return self.title_text

    def populate_localizations(self):
        for nationality in Nationality.objects.all():
            UserCategory.objects.get_or_create(nationality=nationality, universal_user_category=self)

    def save(self, **kwargs):
        super(UniversalUserCategory, self).save()
        self.populate_localizations()


class UserCategory(LocalizedModel):

    universal_user_category = models.ForeignKey(UniversalUserCategory)
    title_text = models.CharField('title', default='... no translation available ...', max_length=200)

    def __unicode__(self):
        return self.title_text


class UserCategoryComponent(VersionedModel):

    category = models.ForeignKey(UniversalUserCategory, default=None)
    universal_habit_meta = models.ForeignKey(UniversalHabitMeta, default=None)
    max_days_int = models.IntegerField('max days', default=0, validators=[MinValueValidator(0), MaxValueValidator(7)],
                                       help_text='The maximum amount of days per week this habit may take place '
                                                 'for the category to be assigned.')

    def to_json(self):
        return {
            'habit_id': self.universal_habit_meta.id,
            'max_days': self.max_days_int
        }