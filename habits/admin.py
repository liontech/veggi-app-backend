from django.contrib import admin
from veggiapp.admin import admin_site
from models import HabitMeta, UniversalUserCategory, UserCategoryComponent, UniversalHabitMeta, UserCategory
from localization.admin import LocalizedModelAdmin, LocalizedStackedInline


class UniversalHabitMetaAdmin(admin.ModelAdmin):
    fields = ('title_text', 'icon')


class HabitMetaAdmin(LocalizedModelAdmin):
    fields = ('nationality', 'universal_habit_meta', 'title_text', 'info_text', 'average_value_int')
    list_display = ('title_text', 'universal_habit_meta', 'nationality')

    def get_readonly_fields(self, request, obj=None):
        return ['universal_habit_meta', 'nationality']


class UserCategoryAdmin(LocalizedModelAdmin):
    fields = ('nationality', 'universal_user_category', 'title_text')
    list_display = ('title_text', 'universal_user_category', 'nationality')

    def get_readonly_fields(self, request, obj=None):
        return ['universal_user_category', 'nationality']


class UserCategoryComponentInline(admin.StackedInline):
    fields = ('category', 'universal_habit_meta', 'max_days_int')
    model = UserCategoryComponent
    extra = 0


class UniversalUserCategoryAdmin(admin.ModelAdmin):
    fields = ('title_text', 'priority_int')
    inlines = [UserCategoryComponentInline]
    list_display = ('title_text', 'priority_int')


admin_site.register(UniversalHabitMeta, UniversalHabitMetaAdmin)
admin_site.register(HabitMeta, HabitMetaAdmin)
admin_site.register(UniversalUserCategory, UniversalUserCategoryAdmin)
admin_site.register(UserCategory, UserCategoryAdmin)