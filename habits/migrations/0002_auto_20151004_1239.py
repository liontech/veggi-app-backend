# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('habits', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='habitmeta',
            name='title_text',
            field=models.CharField(default=b'... no translation available ...', max_length=200, verbose_name=b'title'),
        ),
        migrations.AlterField(
            model_name='usercategory',
            name='title_text',
            field=models.CharField(default=b'... no translation available ...', max_length=200, verbose_name=b'title'),
        ),
    ]
