# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('localization', '__first__'),
        ('content', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='HabitMeta',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title_text', models.CharField(default=b'', max_length=200, verbose_name=b'title')),
                ('info_text', models.TextField(max_length=1600, verbose_name=b'info')),
                ('average_value_int', models.IntegerField(default=3, help_text=b'The amount of days per week an average person would perform this habit.', verbose_name=b'average day count', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(7)])),
                ('nationality', models.ForeignKey(default=None, to='localization.Nationality', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='UniversalHabitMeta',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title_text', models.CharField(default=b'', max_length=200, verbose_name=b'title')),
                ('icon', models.ForeignKey(default=None, to='content.Icon', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='UniversalUserCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title_text', models.CharField(max_length=200, verbose_name=b'title')),
                ('priority_int', models.IntegerField(default=0, verbose_name=b'priority')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='UserCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title_text', models.CharField(max_length=200, verbose_name=b'title')),
                ('nationality', models.ForeignKey(default=None, to='localization.Nationality', null=True)),
                ('universal_user_category', models.ForeignKey(to='habits.UniversalUserCategory')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='UserCategoryComponent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('max_days_int', models.IntegerField(default=0, help_text=b'The maximum amount of days per week this habit may take place for the category to be assigned.', verbose_name=b'max days', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(7)])),
                ('category', models.ForeignKey(default=None, to='habits.UniversalUserCategory')),
                ('universal_habit_meta', models.ForeignKey(default=None, to='habits.UniversalHabitMeta')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='habitmeta',
            name='universal_habit_meta',
            field=models.ForeignKey(default=None, to='habits.UniversalHabitMeta', null=True),
        ),
    ]
